# remark-linkify-regex

Given markdown text nodes, find textual matches of a RegExp, and replace those matches with link nodes.

## Installation

```sh
npm install remark-linkify-regex
```

## Usage

Note: the API is not a remark plugin directly, instead it's a factory for a plugin. You must call the factory with a regex.

```js
// ...
var linkifyRegex = require("remark-linkify-regex");

const urlCallback = matches => "http://example.com/issue/" + matches[1];

unified()
    .use(parse)
    .use(linkifyRegex(/\@[a-z0-9]+\b/i))
    .use(linkifyRegex(/Issue (\d+)\b/, urlCallback));
// ...
```

Markdown document:

```
# Title

This is my friend: @6ilZq3kN0F

Issue 1234 will be linked with custom URL, but issue 1234 won't.

This is redundantly linked: [@6ilZq3kN0F](@6ilZq3kN0F)

cc [@alreadyLinked](@2RNGJafZt)
```

Input AST:

```
root[5] (1:1-11:1, 0-196)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title" (2:3-2:8, 3-8)
├─ paragraph[1] (4:1-4:31, 10-40)
│  └─ text: "This is my friend: @6ilZq3kN0F" (4:1-4:31, 10-40)
├─ paragraph[1] (6:1-6:65, 42-106)
│  └─ text: "Issue 1234 will be linked with custom URL, but issue 1234 won't." (6:1-6:65, 42-106)
├─ paragraph[2] (8:1-8:55, 108-162)
│  ├─ text: "This is redundantly linked: " (8:1-8:29, 108-136)
│  └─ link[1] (8:29-8:55, 136-162) [url="@6ilZq3kN0F"]
│     └─ text: "@6ilZq3kN0F" (8:30-8:41, 137-148)
└─ paragraph[2] (10:1-10:32, 164-195)
   ├─ text: "cc " (10:1-10:4, 164-167)
   └─ link[1] (10:4-10:32, 167-195) [url="@2RNGJafZt"]
      └─ text: "@alreadyLinked" (10:5-10:19, 168-182)
```

Output AST:

```
root[5] (1:1-11:1, 0-196)
├─ heading[1] (2:1-2:8, 1-8) [depth=1]
│  └─ text: "Title"
├─ paragraph[2] (4:1-4:31, 10-40)
│  ├─ text: "This is my friend: "
│  └─ link[1] [url="@6ilZq3kN0F"]
│     └─ text: "@6ilZq3kN0F"
├─ paragraph[2] (6:1-6:65, 42-106)
│  ├─ link[1] [url="http://example.com/issue/1234"]
│  │  └─ text: "Issue 1234"
│  └─ text: " will be linked with custom URL, but issue 1234 won't."
├─ paragraph[2] (8:1-8:55, 108-162)
│  ├─ text: "This is redundantly linked: "
│  └─ link[1] (8:29-8:55, 136-162) [url="@6ilZq3kN0F"]
│     └─ text: "@6ilZq3kN0F" (8:30-8:41, 137-148)
└─ paragraph[2] (10:1-10:32, 164-195)
   ├─ text: "cc "
   └─ link[1] (10:4-10:32, 167-195) [url="@2RNGJafZt"]
      └─ text: "@alreadyLinked" (10:5-10:19, 168-182)
```

## License

[MIT](LICENSE)
