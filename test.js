const test = require("tape");
const unified = require("unified");
const remarkParse = require("remark-parse");
const inspect = require("unist-util-inspect");
const linkifyRegex = require("./index");

test("it lifts a nested list to the root level", (t) => {
  t.plan(2);

  const markdown = `
# Title

This is my friend: @6ilZq3kN0F

Issue 1234 will be linked with custom URL, but issue 1234 won't.

This is redundantly linked: [@6ilZq3kN0F](@6ilZq3kN0F)

cc [@alreadyLinked](@2RNGJafZt)
`;

  const actualInput = unified()
    .use(remarkParse, { commonmark: true })
    .parse(markdown);

  // console.log(JSON.stringify(actualInput));
  console.log(inspect(actualInput));

  const expectedInput = {
    type: "root",
    children: [
      {
        type: "heading",
        depth: 1,
        children: [
          {
            type: "text",
            value: "Title",
            position: {
              start: { line: 2, column: 3, offset: 3 },
              end: { line: 2, column: 8, offset: 8 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 2, column: 1, offset: 1 },
          end: { line: 2, column: 8, offset: 8 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          {
            type: "text",
            value: "This is my friend: @6ilZq3kN0F",
            position: {
              start: { line: 4, column: 1, offset: 10 },
              end: { line: 4, column: 31, offset: 40 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 4, column: 1, offset: 10 },
          end: { line: 4, column: 31, offset: 40 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          {
            type: "text",
            value:
              "Issue 1234 will be linked with custom URL, but issue 1234 won't.",
            position: {
              start: { line: 6, column: 1, offset: 42 },
              end: { line: 6, column: 65, offset: 106 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 6, column: 1, offset: 42 },
          end: { line: 6, column: 65, offset: 106 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          {
            type: "text",
            value: "This is redundantly linked: ",
            position: {
              start: { line: 8, column: 1, offset: 108 },
              end: { line: 8, column: 29, offset: 136 },
              indent: [],
            },
          },
          {
            type: "link",
            title: null,
            url: "@6ilZq3kN0F",
            children: [
              {
                type: "text",
                value: "@6ilZq3kN0F",
                position: {
                  start: { line: 8, column: 30, offset: 137 },
                  end: { line: 8, column: 41, offset: 148 },
                  indent: [],
                },
              },
            ],
            position: {
              start: { line: 8, column: 29, offset: 136 },
              end: { line: 8, column: 55, offset: 162 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 8, column: 1, offset: 108 },
          end: { line: 8, column: 55, offset: 162 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          {
            type: "text",
            value: "cc ",
            position: {
              start: { line: 10, column: 1, offset: 164 },
              end: { line: 10, column: 4, offset: 167 },
              indent: [],
            },
          },
          {
            type: "link",
            title: null,
            url: "@2RNGJafZt",
            children: [
              {
                type: "text",
                value: "@alreadyLinked",
                position: {
                  start: { line: 10, column: 5, offset: 168 },
                  end: { line: 10, column: 19, offset: 182 },
                  indent: [],
                },
              },
            ],
            position: {
              start: { line: 10, column: 4, offset: 167 },
              end: { line: 10, column: 32, offset: 195 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 10, column: 1, offset: 164 },
          end: { line: 10, column: 32, offset: 195 },
          indent: [],
        },
      },
    ],
    position: {
      start: { line: 1, column: 1, offset: 0 },
      end: { line: 11, column: 1, offset: 196 },
    },
  };
  t.deepEquals(actualInput, expectedInput);

  const urlCallback = (matches) => "http://example.com/issue/" + matches[1];
  const actualOutput = linkifyRegex(/Issue (\d+)\b/, urlCallback)()(
    linkifyRegex(/\@[a-z0-9]+\b/i)()(actualInput)
  );

  // console.log(JSON.stringify(actualOutput));
  console.log(inspect(actualOutput));

  const expectedOutput = {
    type: "root",
    children: [
      {
        type: "heading",
        depth: 1,
        children: [{ type: "text", value: "Title" }],
        position: {
          start: { line: 2, column: 1, offset: 1 },
          end: { line: 2, column: 8, offset: 8 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          { type: "text", value: "This is my friend: " },
          {
            type: "link",
            title: null,
            url: "@6ilZq3kN0F",
            children: [{ type: "text", value: "@6ilZq3kN0F" }],
          },
        ],
        position: {
          start: { line: 4, column: 1, offset: 10 },
          end: { line: 4, column: 31, offset: 40 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          {
            type: "link",
            title: null,
            url: "http://example.com/issue/1234",
            children: [{ type: "text", value: "Issue 1234" }],
          },
          {
            type: "text",
            value: " will be linked with custom URL, but issue 1234 won't.",
          },
        ],
        position: {
          start: { line: 6, column: 1, offset: 42 },
          end: { line: 6, column: 65, offset: 106 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          { type: "text", value: "This is redundantly linked: " },
          {
            type: "link",
            title: null,
            url: "@6ilZq3kN0F",
            children: [
              {
                type: "text",
                value: "@6ilZq3kN0F",
                position: {
                  start: { line: 8, column: 30, offset: 137 },
                  end: { line: 8, column: 41, offset: 148 },
                  indent: [],
                },
              },
            ],
            position: {
              start: { line: 8, column: 29, offset: 136 },
              end: { line: 8, column: 55, offset: 162 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 8, column: 1, offset: 108 },
          end: { line: 8, column: 55, offset: 162 },
          indent: [],
        },
      },
      {
        type: "paragraph",
        children: [
          { type: "text", value: "cc " },
          {
            type: "link",
            title: null,
            url: "@2RNGJafZt",
            children: [
              {
                type: "text",
                value: "@alreadyLinked",
                position: {
                  start: { line: 10, column: 5, offset: 168 },
                  end: { line: 10, column: 19, offset: 182 },
                  indent: [],
                },
              },
            ],
            position: {
              start: { line: 10, column: 4, offset: 167 },
              end: { line: 10, column: 32, offset: 195 },
              indent: [],
            },
          },
        ],
        position: {
          start: { line: 10, column: 1, offset: 164 },
          end: { line: 10, column: 32, offset: 195 },
          indent: [],
        },
      },
    ],
    position: {
      start: { line: 1, column: 1, offset: 0 },
      end: { line: 11, column: 1, offset: 196 },
    },
  };
  t.deepEquals(actualOutput, expectedOutput);
});
